/*
  Analog Input

  Demonstrates analog input by reading an analog sensor on analog pin 0 and
  turning on and off a light emitting diode(LED) connected to digital pin 13.
  The amount of time the LED will be on and off depends on the value obtained
  by analogRead().

  The circuit:
  - potentiometer
    center pin of the potentiometer to the analog input 0
    one side pin (either one) to ground
    the other side pin to +5V
  - LED
    anode (long leg) attached to digital output 13
    cathode (short leg) attached to ground

  - Note: because most Arduinos have a built-in LED attached to pin 13 on the
    board, the LED is optional.

  created by David Cuartielles
  modified 30 Aug 2011
  By Tom Igoe

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/AnalogInput
*/
#include <ESP8266WiFi.h>
#include <ArduinoOTA.h>

const char* ssid = "Network Name";
const char* password = "Network Password";

int sensorPin = A0;    // select the input pin for the potentiometer
int ledPin = D5;      // select the pin for the LED
int IR_sensor = D8;
int sensorValue = 0;  // variable to store the value coming from the sensor
int timeValue;
volatile int sensor_flag = 0;

void setup() {
  // declare the ledPin as an OUTPUT:
  pinMode(ledPin, OUTPUT);
  Serial.begin(9600);
  
 //Attach Interrupt
  attachInterrupt(digitalPinToInterrupt(IR_sensor), InterruptCallback, RISING);

  // set WiFi to station mode and disconnect from an AP if previously connected
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);

  
  // attempt to connect to WiFi network
  Serial.println();
  Serial.print("Connecting to WiFi...");
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }

  // show WiFi status in serial monitor
  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  ArduinoOTA.begin();
}

void loop() {
  ArduinoOTA.handle();
 
  if(sensor_flag){
    //digitalWrite(ledPin, HIGH);
    Serial.println("Proximity Sensor Triggered");
    delay(timeValue);
    // digitalWrite(dispensor, LOW);
    digitalWrite(ledPin, LOW);
    Serial.println("Sanitizer Dipensed");
    delay(5000);
    sensor_flag = 0;
    //digitalWrite(ledPin, HIGH);
    //Serial.println(timeValue);
    }


  // read the value from the sensor:
  sensorValue = analogRead(sensorPin);
  Serial.println(sensorValue);
  timeValue = sensorValue * 3;
  // turn the ledPin on
  }


ICACHE_RAM_ATTR void InterruptCallback(){
    sensor_flag = 1;
    digitalWrite(ledPin, HIGH);
}
