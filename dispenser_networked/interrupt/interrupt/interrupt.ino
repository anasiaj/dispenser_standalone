#define Switch1pin 3 //corresponds to PCINT19
#define Switch2pin 4 //corresponds to PCINT19
#define Switch3pin 5 //corresponds to PCINT19
#define Switch4pin 6 //corresponds to PCINT19
#define Switch5pin 7 //corresponds to PCINT19
//These pins are part of PORTD, which can trigger pin change interrupt 2 (PCI2)
//This means that we need to use Pin Change Interrupt Control Register (PCICR) - (-/-/-/-/-/PCIE2/PCIE1/PCIE0); set PCIE2=1 to enable PCI on PD
//and PIN Change Mask Register 2 (PCMSK2) - selects which pins are enabled to trigger the PCI (PCINT23/PCINT22/PCINT21/PCINT20/PCINT19/PCINT18/PCINT17/PCINT16)
// Corresponding to Arduino pins:(7/6/5/4/3/2/1/0)
//Note: pins 1 and 0 are used for the serial bus, i.e. they cannot be used if serial communication is used.

//Pin Change Interrupt Flag Register (PCIFR) - This register contains the flags that trigger interrupt- we do not have anything with it.

//this defines two convenient 'function macros' that allow the setting or clearing of individual bits in a register.
//Function macros are like normal #defines, but arguments in the expression are treated like place holders for actual variables to be inserted by the compiler

#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit)) //clear bit in byte at sfr address
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))//set bit in byte at sfr address

// variables used in the Interrupt Service Routine (ISR) need to be 'volatile'
volatile long unsigned eventTime;
volatile long unsigned previousEventTime;
volatile long unsigned timeSinceLastEvent;
volatile byte portDstatus;
volatile byte eventFlag;

void setup(){
  Serial.begin(9600);

  //define the pins
  pinMode(Switch1pin, INPUT); //define pin as INPUT
  digitalWrite(Switch1pin, HIGH); //connect the internal pull-up resistor - this will yield a HIGH readout if the switch is open.
  pinMode(Switch2pin, INPUT);
  digitalWrite(Switch2pin, HIGH);
  pinMode(Switch3pin, INPUT);
  digitalWrite(Switch3pin, HIGH);
  pinMode(Switch4pin, INPUT);
  digitalWrite(Switch4pin, HIGH);
  pinMode(Switch5pin, INPUT);
  digitalWrite(Switch5pin, HIGH);

  //setting up the interrupt: 
  sbi (PCICR, PCIE2); //enable interrupt PCI2
  sbi (PCMSK2, PCINT23); //set the interrupt control bits for pins 7-3. These bits control whether the pins can trigger interrupt or not. Interrupt is enabled when the bit is set
  sbi (PCMSK2, PCINT22);
  sbi (PCMSK2, PCINT21);
  sbi (PCMSK2, PCINT20);
  sbi (PCMSK2, PCINT19);
}

void loop() {

  if (eventFlag == 1 && (eventTime-previousEventTime)>100){//button event causes eventFlag to be set in ISR. We only consider events occuring 100ms after the last one.
                                                          //This debounces the button.
      timeSinceLastEvent = eventTime - previousEventTime;//calculate the time since last event
      previousEventTime=eventTime;//note the present event time as last time for the the next event.
      Serial.print(timeSinceLastEvent);
      Serial.print(" ms");
      Serial.println(portDstatus,BIN); //print the state of Port D as BINary number
                                       //in a 'real' program you would evaluate portDstatus and find out which button was pressed.
                                       //This could be done in a 'case' structure or with if....then
      eventFlag = 0; //reset the event flag for the next event
  }
}

ISR (PCINT2_vect) {//Interrupt Service Routine. This code is executed whenever a button event (either pressing or releasing) occurs.

eventTime=millis();
eventFlag=1;

}
