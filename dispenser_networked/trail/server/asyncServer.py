# https://www.mendrugory.com/post/python3-7-asyncio-chat/

import asyncio
import time

async def handle(reader, writer):
    addr = writer.get_extra_info('peername')
    message = f"{addr!r} is connected !!!!"
    print(message)
    while True:
        data = await reader.read(100)
        message = data.decode().strip()
        print(f"Data type of data is {type(data)}")
		#bytes("Echo from Server:>", 'utf-8')
        #writer.write(bytes("Echo from Server:> ", 'utf-8'))
        writer.write(bytes("Echo from Server:> ", 'utf-8') + data)
        await writer.drain()
        localtime = time.asctime( time.localtime(time.time()) )
		
        print(f"msg recieved:> {message},Time={localtime},Server={addr!r}")
		#print(f"msg recieved")
        if message == "exit":
            message = f"{addr!r} wants to close the connection."
            print(message)
            break
    writer.close()

async def main():
    server = await asyncio.start_server(
        handle, '127.0.0.1', 8888)
    addr = server.sockets[0].getsockname()
    print(f'Serving on {addr}')
    async with server:
        await server.serve_forever()

asyncio.run(main())