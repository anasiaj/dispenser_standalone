#include<iostream>
#include<sys/types.h>
#include<unistd.h>
#include<sys/socket.h>
#include<netdb.h>
#include<arpa/inet.h>
#include<string.h>
#include<string>

using namespace std;


int main()
{
    //create a socket
    //bind the socket to IP/Port
    //Mark the socket for Listening in
    //Accept a call
    //Close the listening socket
    //While receiving displace message, echo message
    //Close socket
    
    //create a socket
    int listening = socket(AF_INET, SOCK_STREAM, 0);
    if (listening == -1)
    {
	    cerr << "Can't create a socket!";
    }

    //bind the socket to IP/Port
    sockaddr_in hint;
    hint.sin_family = AF_INET;
    hint.sin_port = htons(54000);
    inet_pton(AF_INET, "0.0.0.0", &hint.sin_addr); //127.0.0.1 
    
    if (bind(listening, (sockaddr*)&hint, sizeof(hint)) == -1)
    {
	cerr << "Can't bind to IP/port";
	return -2;
    }   

    //Mark the socket for Listening in
    if (listen(listening, SOMAXCONN) == -1)
    {
	    cerr << "Can't listen!";
	    return -3;
    }

    //Accept a call
    sockaddr_in client;
    socklen_t clientSize = sizeof(client);
    char host[NI_MAXHOST];
    char svc[NI_MAXSERV];

    int clientSocket = accept(listening, (sockaddr*)&client, &clientSize);
    if (clientSocket == -1)
    {
	    cerr << "Problem with client connecting!";
	    return -4;
    }

    //Close the listening socket
    close(listening);

    memset(host, 0, NI_MAXHOST);
    memset(svc, 0, NI_MAXSERV);

    int result = getnameinfo((sockaddr*)&client, sizeof(client), host, NI_MAXHOST, svc, NI_MAXSERV, 0);

    if (result)
    {
	    cout << host << " connected on " << svc << endl;	    
    }
    else
    {
	    inet_ntop(AF_INET, &client.sin_addr, host, NI_MAXHOST);
	    cout << host << " connected on " << ntohs(client.sin_port) << endl;
    }


    //While receiving display message, echo message
    char buf[4096];

    while (true)
    {
	    memset(buf, 0, 4096);
		
        // Wait for client to send data
        int bytesReceived = recv(clientSocket, buf, 4096, 0);
        if (bytesReceived == -1)
        {
            cerr << "Error in recv(). Quitting" << endl;
            break;
        }
 
        if (bytesReceived == 0)
        {
            cout << "Client disconnected " << endl;
            break;
        }
 
        cout << string(buf, 0, bytesReceived) << endl;
 
        // Echo message back to client
        send(clientSocket, buf, bytesReceived + 1, 0);
    }

    // Close the socket
    close(clientSocket);
 
return 0;    
}

