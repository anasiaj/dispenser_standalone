
// This uses Serial Monitor to display Range Finder distance readings

// Hook up HC-SR04 with Trig to Arduino Pin 10, Echo to Arduino pin 13

#include <TimerOne.h>

#define trigPin 5
#define echoPin 6
#define LED_red 14
#define LED_yellow 15
#define LED_blue_3 16
#define LED_blue_2 17
#define LED_blue_1 18
#define buzzer 12
#define IR_sensor 4
#define dispensor 3
#define speedSetting A0
#define pump_motor_control A1

float duration, distance;
int motor_speed;
int IR_sensor_state = 0;
int IR_sensor_active_time;
int pump_on_time;

int dispense_state = 0;

void container_level_update(void);

void setup() {
  Serial.begin (9600);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(LED_red, OUTPUT);
  pinMode(LED_yellow, OUTPUT);
  pinMode(LED_blue_1, OUTPUT);
  pinMode(LED_blue_2, OUTPUT);
  pinMode(LED_blue_3, OUTPUT);
  pinMode(buzzer, OUTPUT);
  pinMode(IR_sensor, INPUT);
  pinMode(dispensor, OUTPUT);
  pinMode(speedSetting, INPUT);
  pinMode(pump_motor_control, OUTPUT);
  Serial.println("Arduino Intializing...");

  //software interrupt
  Timer1.initialize(10000000); //call interrupt every 10 secs.
  Timer1.attachInterrupt(container_level_update); 
  
}

void loop() {
  motor_speed = analogRead(speedSetting)/4;
  dispense_sanitizer(IR_sensor, &IR_sensor_state, &IR_sensor_active_time);
  
  
 /*if (digitalRead(IR_sensor)==LOW){
  
  digitalWrite(dispensor, HIGH);
  analogWrite(pump_motor_control, motor_speed);
  Serial.println("Proximity Sensor Triggered");
  delay(300);
  digitalWrite(dispensor, LOW);
  analogWrite(pump_motor_control, 0);
  Serial.println("Sanitizer Dipensed");
  delay(5000);
  }*/
 
}

void container_level_update(void){

  // Write a pulse to the HC-SR04 Trigger Pin
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  
  // Measure the response from the HC-SR04 Echo Pin
 
  duration = pulseIn(echoPin, HIGH);
  
  // Determine distance from duration
  // Use 343 metres per second as speed of sound
  
  distance = 1.66 + (duration / 2) * 0.0350;

  if ( distance < 5){
    //full tank state
    digitalWrite(LED_blue_1, HIGH);
    digitalWrite(LED_blue_2, LOW);
    digitalWrite(LED_blue_3, LOW);
    digitalWrite(LED_yellow, LOW);
    digitalWrite(LED_red, LOW);
    // Send results to Serial Monitor
    
    Serial.println("Tank level 80% to 100%");
    Serial.print("Depth = ");
    Serial.print(distance);
    Serial.println(" cm");
    delay(500); 
  }
  
  if (distance > 5 && distance < 10){
    //switch off blue led 1 and all other leds on
    digitalWrite(LED_blue_1, LOW);
    digitalWrite(LED_blue_2, HIGH);
    digitalWrite(LED_blue_3, LOW);
    digitalWrite(LED_yellow, LOW);
    digitalWrite(LED_red, LOW);
    // Send results to Serial Monitor
    
    Serial.println("Tank level 60% to 80%");
    Serial.print("Depth = ");
    Serial.print(distance);
    Serial.println(" cm");
    delay(500); 
  }

 if (distance > 10 && distance < 15){
    //switch off blue led 1 and blue led 2  and all other leds on 
    digitalWrite(LED_blue_1, LOW);
    digitalWrite(LED_blue_2, LOW);
    digitalWrite(LED_blue_3, HIGH);
    digitalWrite(LED_yellow, LOW);
    digitalWrite(LED_red, LOW);
    
    Serial.println("Tank level 40% to 60%");
    Serial.print("Depth = ");
    Serial.print(distance);
    Serial.println(" cm");
    delay(500); 
  }

 if (distance > 15 && distance < 20){
    //switch off all blue LEDs and ON other LEDs
    digitalWrite(LED_blue_1, LOW);
    digitalWrite(LED_blue_2, LOW);
    digitalWrite(LED_blue_3, LOW);
    digitalWrite(LED_yellow, LOW);
    digitalWrite(LED_red, HIGH);
    // Send results to Serial Monitor
    
    Serial.println("Tank level 20% to 40%");
    Serial.print("Depth = ");
    Serial.print(distance);
    Serial.println(" cm");
    delay(500); 
  }



  if (distance > 20 && distance < 25){
    //Empty tank state
    //ON red and ring buzzer, off all other LEDS
    digitalWrite(LED_blue_1, LOW);
    digitalWrite(LED_blue_2, LOW);
    digitalWrite(LED_blue_3, LOW);
    digitalWrite(LED_yellow, LOW);
    digitalWrite(LED_red, HIGH);
    digitalWrite(buzzer, HIGH);
    delay(500);
    digitalWrite(buzzer, LOW);
    // Send results to Serial Monitor
    
    Serial.println("Tank Empty");
    Serial.println("Tank level 0% to 20%");
    Serial.print("Depth = ");
    Serial.print(distance);
    Serial.println(" cm");
    delay(500); 
       
  }
  
}


void dispense_sanitizer(int sensor, int* sensor_state, int* sensor_active_time) {
  int status_dsp;
   switch (*sensor_state) {
    case 0:
      if (digitalRead(sensor) == LOW) {
        *sensor_active_time = millis();
        *sensor_state = 1;
        }
        break;
    case 1:
      if ((millis()- *sensor_active_time) > 75){  //hold time of 75ms assumed, value can be increased or decreased based on the sensor quality 
        *sensor_state = 2;
        }
        break;
      
    case 2:
      if (digitalRead(sensor) == HIGH) {
          *sensor_state = 3;
        }
        break;

    case 3:
      status_dsp = dispense();
      if (status_dsp == 1) {
        *sensor_state = 0;
      }
      break;
            
  }
   
}

int dispense(){
  int dispense_status;
  switch(dispense_state) {
    case 0:
      analogWrite(pump_motor_control, motor_speed);
      Serial.println("Proximity Sensor Triggered");
      pump_on_time = millis();
      dispense_status = 0;
      dispense_state = 1;
      break;

     case 1:
      if ((millis()-pump_on_time) > 500){
        analogWrite(pump_motor_control, 0);
        Serial.println("Sanitizer Dispensing completed");
        dispense_status = 1;
        dispense_state = 0;
      }
      break;
  }

   return dispense_status;
}
