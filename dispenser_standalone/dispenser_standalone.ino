
// This uses Serial Monitor to display Range Finder distance readings

// Hook up HC-SR04 with Trig to Arduino Pin 10, Echo to Arduino pin 13

#define trigPin 10
#define echoPin 13
#define LED_red 2
#define LED_yellow 3
#define LED_blue_1 4
#define LED_blue_2 5
#define LED_blue_3 6
#define buzzer 7
#define IR_sensor 8
#define dispensor 9

float duration, distance;

void setup() {
  Serial.begin (9600);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(LED_red, OUTPUT);
  pinMode(LED_yellow, OUTPUT);
  pinMode(LED_blue_1, OUTPUT);
  pinMode(LED_blue_2, OUTPUT);
  pinMode(LED_blue_3, OUTPUT);
  pinMode(buzzer, OUTPUT);
  pinMode(IR_sensor, INPUT);
  pinMode(dispensor, OUTPUT);
  Serial.println("Arduino Intializing...");
  
}

void loop() {
   
  // Write a pulse to the HC-SR04 Trigger Pin
  
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  
  // Measure the response from the HC-SR04 Echo Pin
 
  duration = pulseIn(echoPin, HIGH);
  
  // Determine distance from duration
  // Use 343 metres per second as speed of sound
  
  distance = (duration / 2) * 0.0343;

  if ( distance < 3){
    //full tank state
    digitalWrite(LED_blue_1, HIGH);
    digitalWrite(LED_blue_2, HIGH);
    digitalWrite(LED_blue_3, HIGH);
    digitalWrite(LED_yellow, HIGH);
    digitalWrite(LED_red, HIGH);
    // Send results to Serial Monitor
    /*
    Serial.println("Tank level 80% to 100%");
    Serial.print("Depth = ");
    Serial.print(distance);
    Serial.println(" cm");
    delay(500); */
  }
  
  if (distance > 3 && distance < 6){
    //switch off blue led 1 and all other leds on
    digitalWrite(LED_blue_1, LOW);
    digitalWrite(LED_blue_2, HIGH);
    digitalWrite(LED_blue_3, HIGH);
    digitalWrite(LED_yellow, HIGH);
    digitalWrite(LED_red, HIGH);
    // Send results to Serial Monitor
    /*
    Serial.println("Tank level 60% to 80%");
    Serial.print("Depth = ");
    Serial.print(distance);
    Serial.println(" cm");
    delay(500); */
  }

 if (distance > 6 && distance < 9){
    //switch off blue led 1 and blue led 2  and all other leds on 
    digitalWrite(LED_blue_1, LOW);
    digitalWrite(LED_blue_2, LOW);
    digitalWrite(LED_blue_3, HIGH);
    digitalWrite(LED_yellow, HIGH);
    digitalWrite(LED_red, HIGH);
    /*
    Serial.println("Tank level 40% to 60%");
    Serial.print("Depth = ");
    Serial.print(distance);
    Serial.println(" cm");
    delay(500); */
  }

 if (distance > 9 && distance < 12){
    //switch off all blue LEDs and ON other LEDs
    digitalWrite(LED_blue_1, LOW);
    digitalWrite(LED_blue_2, LOW);
    digitalWrite(LED_blue_3, LOW);
    digitalWrite(LED_yellow, HIGH);
    digitalWrite(LED_red, HIGH);
    // Send results to Serial Monitor
    /*
    Serial.println("Tank level 20% to 40%");
    Serial.print("Depth = ");
    Serial.print(distance);
    Serial.println(" cm");
    delay(500); */
  }



  if (distance > 12 && distance < 15){
    //Empty tank state
    //ON red and ring buzzer, off all other LEDS
    digitalWrite(LED_blue_1, LOW);
    digitalWrite(LED_blue_2, LOW);
    digitalWrite(LED_blue_3, LOW);
    digitalWrite(LED_yellow, LOW);
    digitalWrite(LED_red, HIGH);
    digitalWrite(buzzer, HIGH);
    delay(500);
    digitalWrite(buzzer, LOW);
    // Send results to Serial Monitor
    /*
    Serial.println("Tank Empty");
    Serial.println("Tank level 0% to 20%");
    Serial.print("Depth = ");
    Serial.print(distance);
    Serial.println(" cm");
    delay(500); */
       
  }

if (digitalRead(IR_sensor)==LOW){
  digitalWrite(dispensor, HIGH);
  Serial.println("Proximity Sensor Triggered");
  delay(1000);
  digitalWrite(dispensor, LOW);
  Serial.println("Sanitizer Dipensed");
  }
 

   // Send results to Serial Monitor
   //Serial.print("Distance = ");
  //if (distance >= 400 || distance <= 2) {
     //Serial.println("Out of range");
 // }
 // else {
    //Serial.print(distance);
    //Serial.println(" cm");
   // delay(500);
  //}
  //delay(500);
}
