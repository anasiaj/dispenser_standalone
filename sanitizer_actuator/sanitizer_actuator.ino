
// This uses Serial Monitor to display Range Finder distance readings

// Hook up HC-SR04 with Trig to Arduino Pin 10, Echo to Arduino pin 13

#define trigPin 10
#define echoPin 13
#define LED_red 2
#define LED_yellow 3
#define LED_blue_1 4
#define LED_blue_2 5
#define LED_blue_3 6
#define buzzer 7
#define IR_sensor 8
#define dispensor 9

float duration, distance;

void setup() {
  Serial.begin (9600);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(LED_red, OUTPUT);
  pinMode(LED_yellow, OUTPUT);
  pinMode(LED_blue_1, OUTPUT);
  pinMode(LED_blue_2, OUTPUT);
  pinMode(LED_blue_3, OUTPUT);
  pinMode(buzzer, OUTPUT);
  pinMode(IR_sensor, INPUT);
  pinMode(dispensor, OUTPUT);
  Serial.println("Arduino Intializing...");
  
}

void loop() {
   
//digitalRead(IR_sensor);
if (digitalRead(IR_sensor)==LOW){
  digitalWrite(dispensor, HIGH);
  Serial.println("Proximity Sensor Triggered");
  delay(1000);
  digitalWrite(dispensor, LOW);
  Serial.println("Sanitizer Dipensed");
  }
 
}
