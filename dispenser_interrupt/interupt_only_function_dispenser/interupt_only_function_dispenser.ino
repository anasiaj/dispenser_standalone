
//Instrupt coding flow......
//Turn on Pin Change Interrupts
//Chose Which Pin to Interrupt on
//Write ISR

#define dispensor 3
#define IR_sensor 4
#define dispense_duration 1000

#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit)) //clear bit in byte at sfr address
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))//set bit in byte at sfr address


volatile byte eventFlag;
volatile long unsigned dispenser_trigger_time;
volatile byte portDstatus;
int dispenser_state =1;

void dispense(void);

void setup() {
 //Turn on Pin Change Interrupts 
 sbi (PCICR, PCIE2); //enable interrupt PCI2 for PortD
 //Chose Which Pin to Interrupt on
 sbi (PCMSK2, PCINT20); //Pin No. 4 which is PCINT20
 

}

void loop() {
  dispense();

}

void dispense(void){
  switch(dispenser_state){
    case 1:
          if(eventFlag==1){
            dispenser_trigger_time=millis();
            dispenser_state = 2;
            break;            
          }
          break;
    case 2:
          if((millis()-dispenser_trigger_time)>dispense_duration){
            digitalWrite(dispensor, LOW);
            eventFlag=0;
            dispenser_state = 1;
            break;
            }
            break;
  }
  
}

ISR (PCINT2_vect) { 
      portDstatus = PIND; // save the momentory status of PortD pins in to variable
      if (!bitRead(portDstatus, 4)){ // the status of PD4 which is the IR sensor pin
        digitalWrite(dispensor, HIGH);
        eventFlag=1;
        }
      
      }
