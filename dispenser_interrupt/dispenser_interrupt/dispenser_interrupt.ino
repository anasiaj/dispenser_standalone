


// This uses Serial Monitor to display Range Finder distance readings

// Hook up HC-SR04 with Trig to Arduino Pin 10, Echo to Arduino pin 13

//verifi which port is IR_sensor and accordingly set the interrupts

#include <TimerOne.h>

#define trigPin 5
#define echoPin 6
#define LED_red 14
#define LED_yellow 15
#define LED_blue_3 16
#define LED_blue_2 17
#define LED_blue_1 18
#define buzzer 12
#define IR_sensor 4
#define dispensor 3
#define dispense_duration 1000

#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit)) //clear bit in byte at sfr address
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))//set bit in byte at sfr address


float duration, distance;
volatile byte eventFlag;
volatile long unsigned dispenser_trigger_time;
volatile byte portDstatus;
int dispenser_state =1;

void container_level_update(void);
void dispense(void);

void setup() {
    //setting up the interrupt: 
  sbi (PCICR, PCIE2); //enable interrupt PCI2
  sbi (PCMSK2, PCINT19); //set the interrupt control bits for pins 7-3. These bits control whether the pins can trigger interrupt or not. Interrupt is enabled when the bit is set

  Serial.begin (9600);
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  pinMode(LED_red, OUTPUT);
  pinMode(LED_yellow, OUTPUT);
  pinMode(LED_blue_1, OUTPUT);
  pinMode(LED_blue_2, OUTPUT);
  pinMode(LED_blue_3, OUTPUT);
  pinMode(buzzer, OUTPUT);
  pinMode(IR_sensor, INPUT);
  pinMode(dispensor, OUTPUT);
  Serial.println("Arduino Intializing...");

  //software interrupt
  Timer1.initialize(10000000); //call interrupt every 10 secs.
  Timer1.attachInterrupt(container_level_update); 
  
}

void loop() {
 if (digitalRead(IR_sensor)==LOW){
  digitalWrite(dispensor, HIGH);
  Serial.println("Proximity Sensor Triggered");
  delay(1000);
  digitalWrite(dispensor, LOW);
  Serial.println("Sanitizer Dipensed");
  delay(5000);
  }
 
}

void container_level_update(void){

  // Write a pulse to the HC-SR04 Trigger Pin
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  
  // Measure the response from the HC-SR04 Echo Pin
 
  duration = pulseIn(echoPin, HIGH);
  
  // Determine distance from duration
  // Use 343 metres per second as speed of sound
  
  distance = 1.66 + (duration / 2) * 0.0350;

  if ( distance < 5){
    //full tank state
    digitalWrite(LED_blue_1, HIGH);
    digitalWrite(LED_blue_2, LOW);
    digitalWrite(LED_blue_3, LOW);
    digitalWrite(LED_yellow, LOW);
    digitalWrite(LED_red, LOW);
    // Send results to Serial Monitor
    
    Serial.println("Tank level 80% to 100%");
    Serial.print("Depth = ");
    Serial.print(distance);
    Serial.println(" cm");
    delay(500); 
  }
  
  if (distance > 5 && distance < 10){
    //switch off blue led 1 and all other leds on
    digitalWrite(LED_blue_1, LOW);
    digitalWrite(LED_blue_2, HIGH);
    digitalWrite(LED_blue_3, LOW);
    digitalWrite(LED_yellow, LOW);
    digitalWrite(LED_red, LOW);
    // Send results to Serial Monitor
    
    Serial.println("Tank level 60% to 80%");
    Serial.print("Depth = ");
    Serial.print(distance);
    Serial.println(" cm");
    delay(500); 
  }

 if (distance > 10 && distance < 15){
    //switch off blue led 1 and blue led 2  and all other leds on 
    digitalWrite(LED_blue_1, LOW);
    digitalWrite(LED_blue_2, LOW);
    digitalWrite(LED_blue_3, HIGH);
    digitalWrite(LED_yellow, LOW);
    digitalWrite(LED_red, LOW);
    
    Serial.println("Tank level 40% to 60%");
    Serial.print("Depth = ");
    Serial.print(distance);
    Serial.println(" cm");
    delay(500); 
  }

 if (distance > 15 && distance < 20){
    //switch off all blue LEDs and ON other LEDs
    digitalWrite(LED_blue_1, LOW);
    digitalWrite(LED_blue_2, LOW);
    digitalWrite(LED_blue_3, LOW);
    digitalWrite(LED_yellow, LOW);
    digitalWrite(LED_red, HIGH);
    // Send results to Serial Monitor
    
    Serial.println("Tank level 20% to 40%");
    Serial.print("Depth = ");
    Serial.print(distance);
    Serial.println(" cm");
    delay(500); 
  }



  if (distance > 20 && distance < 25){
    //Empty tank state
    //ON red and ring buzzer, off all other LEDS
    digitalWrite(LED_blue_1, LOW);
    digitalWrite(LED_blue_2, LOW);
    digitalWrite(LED_blue_3, LOW);
    digitalWrite(LED_yellow, LOW);
    digitalWrite(LED_red, HIGH);
    digitalWrite(buzzer, HIGH);
    delay(500);
    digitalWrite(buzzer, LOW);
    // Send results to Serial Monitor
    
    Serial.println("Tank Empty");
    Serial.println("Tank level 0% to 20%");
    Serial.print("Depth = ");
    Serial.print(distance);
    Serial.println(" cm");
    delay(500); 
       
  }
  
}

void dispense(void){
  switch(dispenser_state){
    case 1:
          if(eventFlag==1){
            dispenser_state = 2;
            break;            
          }
          break;
    case 2:
          if((millis()-dispenser_trigger_time)>dispense_duration){
            digitalWrite(dispensor, LOW);
            eventFlag=0;
            dispenser_state = 1;
            break;
            }
            break;
  }
  
}

ISR (PCINT2_vect) {//Interrupt Service Routine. This code is executed whenever a  event (either pressing or releasing) occurs.

      dispenser_trigger_time=millis();
      eventFlag=1;
      if (!bitRead(portDstatus, 3)){ //neet to update the bit no. after verifying the interrupt bit no. for  IR sensor
        digitalWrite(dispensor, HIGH);
        }
      
      }
